<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Page

Route::get('/','DashboardController@index');
Route::get('/jabatan','JabatanController@index');
Route::get('/karyawan','KaryawanController@index');
Route::get('/karyawan/create','KaryawanController@create');
Route::get('/karyawan/edit','KaryawanController@edit');
Route::get('/karyawan/detail','KaryawanController@detail');

// Data
Route::get('/jabatan/data-byId','JabatanController@data_byId');
Route::post('/jabatan/store','JabatanController@store');
Route::post('/jabatan/s_edit','JabatanController@update');
Route::post('/jabatan/delete','JabatanController@delete');

Route::post('/karyawan/store','KaryawanController@store');
Route::post('/karyawan/s_edit','KaryawanController@update');
Route::post('/karyawan/delete','KaryawanController@delete');

