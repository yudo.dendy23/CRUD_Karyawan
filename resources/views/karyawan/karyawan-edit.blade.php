<!DOCTYPE html>
<html>
<head>
  @section('title','CRUD KARYAWAN | Edit Karyawan')
  @include('templates.head')

</head>

<body>
  <!-- Sidenav -->
  @include('templates.sidebar')
  <!-- Sidenav -->

  <!-- Main content -->
  <div class="main-content" id="panel">
    
    <!-- Topnav -->
    @include('templates.topbar')
    <!-- Topnav -->

    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0"><i class="fa fa-database"></i> Karyawan</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Data Karyawan</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Tambah Karyawan</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Header -->

    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row mb-3">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
          <a class="btn btn-warning text-white" href="/karyawan"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="card px-3 py-3">
            <div class="row">
              <div class="col-12">
                <h4>Form Tambah Karyawan</h4>
                <form id="frmEditKaryawan">
                  @csrf
                  <input type="hidden" name="id" value="{{$karyawan[0]->id}}">
                  <div class="form-group">
                    <label for="jabatan">Jabatan<span class="text-danger">*</span></label>
                    <select class="form-control" name="id_jabatan">
                      @foreach($jabatan as $index => $row)
                      @if($row->id == $karyawan[0]->id_jabatan)
                        <option value="{{$row->id}}" selected>{{$row->jabatan}}</option>
                      @else
                        <option value="{{$row->id}}">{{$row->jabatan}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="nama" placeholder="Input nama" name="nama" value="{{$karyawan[0]->nama}}">
                  </div>
                  <div class="form-group">
                    <label for="foto">Foto <i>(Opsional)</i></label>
                    <div class="">
                      <img src="{{URL::to('/')}}/images/{{$karyawan[0]->photo}}" width="150" height="150">
                    </div>
                    <input type="file" class="form-control" id="foto" name="foto">
                  </div>
                  <div class="form-group">
                    <label for="tempat_lahir">Tempat lahir<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="tempat_lahir" placeholder="Input tempat_lahir" name="tempat_lahir" value="{{$karyawan[0]->tempat_lahir}}">
                  </div>
                  <div class="form-group">
                    <label for="tanggal_lahir">Tanggal lahir<span class="text-danger">*</span></label>
                    <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="{{$karyawan[0]->tanggal_lahir}}">
                  </div>
                  <div class="form-group">
                    <label for="no_hp">No.Hp<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="Input nomor telephone" value="{{$karyawan[0]->no_hp}}">
                  </div>
                  <div class="form-group">
                    <label for="email">Email<span class="text-danger">*</span></label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Input email" value="{{$karyawan[0]->email}}">
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat<span class="text-danger">*</span></label>
                    <textarea class="form-control" name="alamat" placeholder="Input alamat" rows="5">{{$karyawan[0]->alamat}}</textarea>
                  </div>
                  <div class="form-group">
                    <label class="control-label mb-10">Jenis Kelamin<span class="text-danger">*</span></label>
                    <div>
                      <?php
                        $checked_L = "";
                        $checked_P = "";
                        if($karyawan[0]->jk == "L"){
                          $checked_L = "checked";
                        }
                        else{
                          $checked_P = "checked";
                        }
                      ?>
                      <div class="radio">
                        <input type="radio" name="jk" value="L" <?= $checked_L ?>>
                        <label for="radio_3">
                        Laki-Laki
                        </label>
                      </div>
                      <div class="radio">
                        <input type="radio" name="jk" value="P" <?= $checked_P ?>>
                        <label for="radio_4">
                        Perempuan
                        </label>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      @include('templates.footer')
      <!-- Footer -->
    </div>
    <!-- Page Content -->
  </div>
  <!-- Argon Scripts -->

  <!-- Script -->
  @include('templates.script')
  <script type="text/javascript">
    $(function(){
      $(document).on('submit','#frmEditKaryawan',function(e){
        e.preventDefault();
        let _form = $('#frmEditKaryawan');
        $.ajax({
          url : base_url + 'karyawan/s_edit',
          method : "POST",
          dataType : "JSON",
          data : new FormData($('#frmEditKaryawan')[0]),
          contentType:false,
          cache:false,
          processData:false,
          beforeSend : function(){
            _form.find('button[type="submit"]').html('Loading...');
            _form.find('button[type="submit"]').attr('disabled',true);
          },
          success : function(resp){
            _form.find('button[type="submit"]').html('Simpan');
            _form.find('button[type="submit"]').removeAttr('disabled');

            if(resp.IsError == true){
              showToast('warning','Gagal',resp.Message);
            }
            else{
              swal({   
                title: "Berhasil!",   
                type: "success", 
                text: resp.Message,
                confirmButtonColor: "#469408",   
              })
              .then((value) => {
                $('#modalTambahJabatan').modal('hide');
                location.reload();
              })
            }
          },
          error : function(){
            swal({   
              title: "Koneksi Terputus!",   
              type: "error", 
              text: "Klik tombol dibawah dan halaman akan reload otomatis",
              confirmButtonColor: "#469408",   
            })
            .then((value) => {
              location.reload();
            })
          }
        })
      })
    })
  </script>
  <!-- Script -->
</body>

</html>