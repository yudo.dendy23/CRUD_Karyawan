<!DOCTYPE html>
<html>
<head>
  @section('title','CRUD KARYAWAN | Detail Karyawan')
  @include('templates.head')

</head>

<body>
  <!-- Sidenav -->
  @include('templates.sidebar')
  <!-- Sidenav -->

  <!-- Main content -->
  <div class="main-content" id="panel">
    
    <!-- Topnav -->
    @include('templates.topbar')
    <!-- Topnav -->

    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0"><i class="fa fa-database"></i> Karyawan</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Data Karyawan</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Detail Karyawan</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Header -->

    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row mb-3">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
          <a class="btn btn-warning text-white" href="/karyawan"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-lg-6">
          <div class="card px-3 py-3" style="width: 100%;">
            <img class="card-img-top" src="{{URL::to('/')}}/images/{{$karyawan[0]->photo}}" alt="Card image cap" height="300">
            <div class="card-body">
              <h2 class="card-title text-center mb-0">{{$karyawan[0]->nama}}</h2>
              <h5 class="text-center mt-0"><b>{{$karyawan[0]->jabatan}}</b></h5>
              <hr>
              @if($karyawan[0]->jk == "L")
              <p ><i class="fa fa-male"></i> Jenis Kelamin :<span>Laki-Laki</span></p>
              @else 
              <p ><i class="fa fa-female"></i> Jenis Kelamin :<span>Perempuan</span></p>
              @endif
              <p ><i class="fa fa-building"></i> Tempat_lahir :<span>{{$karyawan[0]->tempat_lahir}}</span></p>
              <p ><i class="fa fa-birthday-cake"></i> Tanggal_lahir :<span>{{$karyawan[0]->tanggal_lahir}}</span></p>
              <p ><i class="fa fa-home"></i> Alamat :<span>{{$karyawan[0]->alamat}}</span></p>
              <p ><i class="fa fa-phone"></i> No.hp :<span>{{$karyawan[0]->no_hp}}</span></p>
              <p ><i class="fa fa-envelope"></i> Email :<span>{{$karyawan[0]->email}}</span></p>
              <a href="/karyawan/edit?id={{$karyawan[0]->id}}" class="btn btn-primary btn-md"><i class="fa fa-edit"></i> Edit Data</a>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      @include('templates.footer')
      <!-- Footer -->
    </div>
    <!-- Page Content -->
  </div>
  <!-- Argon Scripts -->

  <!-- Script -->
  @include('templates.script')
  <!-- Script -->
</body>

</html>