<!DOCTYPE html>
<html>
<head>
  @section('title','CRUD KARYAWAN | Karyawan')
  @include('templates.head')

</head>

<body>
  <!-- Sidenav -->
  @include('templates.sidebar')
  <!-- Sidenav -->

  <!-- Main content -->
  <div class="main-content" id="panel">
    
    <!-- Topnav -->
    @include('templates.topbar')
    <!-- Topnav -->

    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0"><i class="fa fa-database"></i> Karyawan</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Data Karyawan</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Data Karyawan</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Header -->

    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row mb-3">
        <div class="col-3">
          <a class="btn btn-info text-white" href="/karyawan/create"><i class="fa fa-plus"></i> Tambah Data</a>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="card px-3 py-3">
            <div class="row mt-3">
              <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <form class="form-inline" method="get" action="/karyawan">
                  <div class="form-group mr-2 mb-2">
                    <label for="search" class="sr-only">Searching</label>
                    <input type="text" class="form-control" id="search" placeholder="Search keyword" style="width: 300px;" name="search">
                  </div>
                  <div class="form-group mr-2 mb-2">
                    <label for="limit" class="sr-only">Limit</label>
                    <select class="form-control" name="limit">
                      <option value="10">10</option>
                      <option value="20">20</option>
                      <option value="30">30</option>
                      <option value="40">40</option>
                      <option value="50">50</option>
                      <option value="100">100</option>
                    </select>
                  </div>
                  <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i></button>
                </form>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered">
                    <thead>
                      <th>Aksi</th>
                      <th>Nama</th>
                      <th>Photo</th>
                      <th>Jabatan</th>
                      <th>Jenis Kelamin</th>
                      <th>Tempat Lahir</th>
                      <th>Tanggal Lahir</th>
                      <th>Alamat</th>
                      <th>No.hp</th>
                      <th>Email</th>
                      <th>Dibuat pada</th>
                    </thead>
                    <tbody>
                      @if($data->total() == 0)
                        <tr align="center">
                          <td colspan="11">
                            Tidak ada data ditemukan
                          </td>
                        </tr>
                      @endif
                      @foreach($data as $index => $row)
                        <tr>
                          <td>
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Aksi
                              </button>
                              <div class="dropdown-menu">
                                <a href="/karyawan/detail?id={{$row->id}}" class="dropdown-item" data-id="{{$row->id}}"><i class="fa fa-eye" ></i> Lihat detail</a>
                                <a href="/karyawan/edit?id={{$row->id}}" class="btn-edit dropdown-item" data-id="{{$row->id}}"><i class="fa fa-edit" ></i> Edit Data</a>
                                <a href="#" class="btn-delete dropdown-item" data-id="{{$row->id}}"><i class="fa fa-trash"></i> Hapus Data</a>
                              </div>
                            </div>
                          </td>
                          <td>{{$row->nama}}</td>
                          <td>
                            <img src="{{URL::to('/')}}/images/{{$row->photo}}" width="100" height="100">
                          </td>
                          <td>{{$row->jabatan}}</td>
                          <td>
                            @if($row->jk == "L")
                              <i class="fa fa-male"></i> Pria
                            @else
                              <i class="fa fa-female"></i> Wanita
                            @endif
                          </td>
                          <td>{{$row->tempat_lahir}}</td>
                          <td>{{$row->tanggal_lahir}}</td>
                          <td>{{$row->alamat}}</td>
                          <td>{{$row->no_hp}}</td>
                          <td>{{$row->email}}</td>
                          <td>{{$row->created_at}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            @if($data->total() != 0)
            <div class="row mt-3">
              <div class="col-12">
                {{$data->links()}}
              </div>
            </div>
            @endif
          </div>
        </div>
      </div>
      <!-- Footer -->
      @include('templates.footer')
      <!-- Footer -->
    </div>
    <!-- Page Content -->
  </div>
  <!-- Argon Scripts -->

  <!-- Script -->
  @include('templates.script')
  <script type="text/javascript">
    $(function(){
      $(document).on('click','.btn-delete',function(){
        let id = $(this).data('id');
        swal({
          title: "Pemberitahuan?",   
          text: "Apakah kamu yakin ingin menghapus data ini!",   
          type: "warning",  
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            ajaxcsrfscript();
            $.ajax({
              url : base_url + 'karyawan/delete',
              method : "POST",
              dataType : "JSON",
              data : {id:id},
              success : function(resp){

                if(resp.IsError == true){
                  alert(resp.Message);
                }
                else{
                  swal({   
                    title: "Berhasil!",   
                    type: "success", 
                    text: resp.Message,
                    confirmButtonColor: "#469408",   
                  })
                  .then((value) => {
                    location.reload();
                  })
                }
              },
              error : function(){
                swal({   
                  title: "Koneksi Terputus!",   
                  type: "error", 
                  text: "Klik tombol dibawah dan halaman akan reload otomatis",
                  confirmButtonColor: "#469408",   
                })
                .then((value) => {
                  location.reload();
                })
              }
            })
          }
        })
      })
    })
  </script>
  <!-- Script -->
</body>

</html>