<!DOCTYPE html>
<html>
<head>
  @section('title','CRUD KARYAWAN | Jabatan')
  @include('templates.head')

</head>

<body>
  <!-- Sidenav -->
  @include('templates.sidebar')
  <!-- Sidenav -->

  <!-- Main content -->
  <div class="main-content" id="panel">
    
    <!-- Topnav -->
    @include('templates.topbar')
    <!-- Topnav -->

    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0"><i class="fa fa-database"></i> Jabatan</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Data Jabatan</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Data Jabatan</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Header -->

    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row mb-3">
        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-xs-12">
          <a class="btn btn-info text-white btn-modal-tambah" href="#"><i class="fa fa-plus"></i> Tambah Data</a>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="card px-3">
            <div class="row mt-3">
              <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <form class="form-inline" method="get" action="/jabatan">
                  <div class="form-group mr-2 mb-2">
                    <label for="search" class="sr-only">Searching</label>
                    <input type="text" class="form-control" id="search" placeholder="Search keyword" style="width: 300px;" name="search">
                  </div>
                  <div class="form-group mr-2 mb-2">
                    <label for="limit" class="sr-only">Limit</label>
                    <select class="form-control" name="limit">
                      <option value="10">10</option>
                      <option value="20">20</option>
                      <option value="30">30</option>
                      <option value="40">40</option>
                      <option value="50">50</option>
                      <option value="100">100</option>
                    </select>
                  </div>
                  <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i></button>
                </form>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="table-resposive">
                  <table class="table table-striped table-bordered" id="table-data">
                    <thead>
                      <th>Aksi</th>
                      <th>Jabatan</th>
                      <th>Dibuat pada</th>
                    </thead>
                    <tbody>
                      @if($data->total() == 0)
                        <tr align="center">
                          <td colspan="3">
                            Tidak ada data ditemukan
                          </td>
                        </tr>
                      @endif
                      @foreach($data as $index => $row)
                        <tr>
                          <td>
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Aksi
                              </button>
                              <div class="dropdown-menu">
                                <a class="dropdown-item btn-edit" data-id="{{$row->id}}" href="#"><i class="fa fa-edit" ></i> Edit Data</a>
                                <a class="dropdown-item btn-delete" data-id="{{$row->id}}" href="#"><i class="fa fa-trash" ></i> Hapus Data</a>
                              </div>
                            </div>
                          </td>
                          <td>{{$row->jabatan}}</td>
                          <td>{{$row->created_at}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            @if($data->total() != 0)
            <div class="row mt-3">
              <div class="col-12">
                {{$data->links()}}
              </div>
            </div>
            @endif
          </div>
          
        </div>
      </div>
      <!-- Footer -->
      @include('templates.footer')
      <!-- Footer -->
    </div>
    <!-- Page Content -->
  </div>

  <!-- Modal -->
  @include('modal.jabatan')
  <!-- Modal -->

  <!-- Argon Scripts -->

  <!-- Script -->
  @include('templates.script')
  <script type="text/javascript">
    $(function(){
      $(document).on('click','.btn-modal-tambah',function(){
        $('#modalTambahJabatan').modal('show');
      });
      $(document).on('click','.btn-edit',function(){
        let id = $(this).data('id');
        $.ajax({
          url : base_url + 'jabatan/data-byId',
          method : "GET",
          data : {id:id},
          dataType : "JSON",
          success : function(resp){
            if(resp.IsError == false){
              $('#frmEditJabatan').find('input[name="id"]').val(resp.Data[0]["id"]);
              $('#frmEditJabatan').find('input[name="jabatan"]').val(resp.Data[0]["jabatan"]);
              $('#modalEditJabatan').modal('show');
            }
          },
          error : function(){
            swal({   
              title: "Koneksi Terputus!",   
              type: "error", 
              text: "Klik tombol dibawah dan halaman akan reload otomatis",
              confirmButtonColor: "#469408",   
              },function(){
                location.href = location.reload();
            })
          }
        })
      })
      $(document).on('click','.btn-delete',function(){
        let id = $(this).data('id');
        swal({
          title: "Pemberitahuan?",   
          text: "Apakah kamu yakin ingin menghapus data ini!",   
          type: "warning",  
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
          if (result.value) {
            ajaxcsrfscript();
            $.ajax({
              url : base_url + 'jabatan/delete',
              method : "POST",
              dataType : "JSON",
              data : {id:id},
              success : function(resp){

                if(resp.IsError == true){
                  showToast('warning','Gagal',resp.Message);
                }
                else{
                  swal({   
                    title: "Berhasil!",   
                    type: "success", 
                    text: resp.Message,
                    confirmButtonColor: "#469408",   
                  })
                  .then((value) => {
                    location.reload();
                  })
                }
              },
              error : function(){
                swal({   
                  title: "Koneksi Terputus!",   
                  type: "error", 
                  text: "Klik tombol dibawah dan halaman akan reload otomatis",
                  confirmButtonColor: "#469408",   
                })
                .then((value) => {
                  location.reload();
                })
              }
            })
          }
        })
      })
      $(document).on('submit','#frmTambahJabatan',function(e){
        e.preventDefault();
        let _form = $('#frmTambahJabatan');
        $.ajax({
          url : base_url + 'jabatan/store',
          method : "POST",
          dataType : "JSON",
          data : getFormData(_form),
          beforeSend : function(){
            _form.find('button[type="submit"]').html('Loading...');
            _form.find('button[type="submit"]').attr('disabled',true);
          },
          success : function(resp){
            _form.find('button[type="submit"]').html('Simpan');
            _form.find('button[type="submit"]').removeAttr('disabled');

            if(resp.IsError == true){
              showToast('warning','Gagal',resp.Message);
            }
            else{
              swal({   
                title: "Berhasil!",   
                type: "success", 
                text: resp.Message,
                confirmButtonColor: "#469408",   
              })
              .then((value) => {
                $('#modalTambahJabatan').modal('hide');
                location.reload();
              })
            }
          },
          error : function(){
            swal({   
              title: "Koneksi Terputus!",   
              type: "error", 
              text: "Klik tombol dibawah dan halaman akan reload otomatis",
              confirmButtonColor: "#469408",   
            })
            .then((value) => {
              location.reload();
            })
          }
        })
      })
      $(document).on('submit','#frmEditJabatan',function(e){
        e.preventDefault();
        let _form = $('#frmEditJabatan');
        $.ajax({
          url : base_url + 'jabatan/s_edit',
          method : "POST",
          dataType : "JSON",
          data : getFormData(_form),
          beforeSend : function(){
            _form.find('button[type="submit"]').html('Loading...');
            _form.find('button[type="submit"]').attr('disabled',true);
          },
          success : function(resp){
            _form.find('button[type="submit"]').html('Simpan');
            _form.find('button[type="submit"]').removeAttr('disabled');

            if(resp.IsError == true){
              showToast('warning','Gagal',resp.Message);
            }
            else{
              swal({   
                title: "Berhasil!",   
                type: "success", 
                text: resp.Message,
                confirmButtonColor: "#469408",   
              })
              .then((value) => {
                $('#modalEditJabatan').modal('hide');
                location.reload();
              })
            }
          },
          error : function(){
            swal({   
              title: "Koneksi Terputus!",   
              type: "error", 
              text: "Klik tombol dibawah dan halaman akan reload otomatis",
              confirmButtonColor: "#469408",   
              },function(){
                location.href = location.reload();
            })
          }
        })
      })
    })
  </script>
  <!-- Script -->

</body>

</html>