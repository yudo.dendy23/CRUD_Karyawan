<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
  <div class="scrollbar-inner">
    <!-- Brand -->
    <div class="sidenav-header  align-items-center">
      <a class="navbar-brand" href="javascript:void(0)">
        <img src="{{ URL::to('/')}}/templating/img/brand/blue.png" class="navbar-brand-img" alt="...">
      </a>
    </div>
    <div class="navbar-inner">
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Heading -->
        <h6 class="navbar-heading p-0 text-muted">
          <span class="docs-normal">Master Data</span>
        </h6>
        <!-- Navigation -->
        <ul class="navbar-nav mb-md-3">
          <li class="nav-item">
            <a class="nav-link" href="{{url('/')}}" >
              <i class="ni ni-tv-2 text-primary"></i>
              <span class="nav-link-text">Dashboard</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/jabatan')}}" >
              <i class="fa fa-tags"></i>
              <span class="nav-link-text">Jabatan</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/karyawan')}}" >
              <i class="fa fa-male"></i>
              <span class="nav-link-text">Karyawan</span>
            </a>
          </li>
          
        </ul>
      </div>
    </div>
  </div>
</nav>