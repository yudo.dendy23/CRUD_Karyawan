<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JabatanModel extends Model
{
	protected $guarded = [];
    protected $table = "jabatan";

}
