<?php

namespace App\Http\Controllers;

use App\KaryawanModel;
use App\JabatanModel;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Storage;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data["data"] = KaryawanModel::leftJoin('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id')
                                       ->select('karyawan.*', 'jabatan.jabatan')
                                       ->orderBy('id','DESC')
                                       ->paginate(10);

        if(!empty($request->input('search'))){
            $data["data"] = KaryawanModel::leftJoin('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id')
                                           ->select('karyawan.*', 'jabatan.jabatan')
                                           ->where('karyawan.nama',"like","%".trim(htmlentities($request->input('search')))."%")
                                           ->orderBy('id','DESC')
                                           ->paginate(10)
                                           ->appends('search',trim(htmlentities($request->input('search'))));

            if(!empty($request->input('limit'))){
                $data["data"] = KaryawanModel::leftJoin('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id')
                                               ->select('karyawan.*', 'jabatan.jabatan')
                                               ->where('karyawan.nama',"like","%".trim(htmlentities($request->input('search')))."%")
                                               ->orderBy('id','DESC')
                                               ->paginate(trim(htmlentities($request->input('limit'))))
                                               ->appends(['search' => trim(htmlentities($request->input('search'))),'limit' => trim(htmlentities($request->input('limit')))]);
            }
        }
        else{
            if(!empty($request->input('limit'))){
                $data["data"] = KaryawanModel::leftJoin('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id')
                                               ->select('karyawan.*', 'jabatan.jabatan')
                                               ->where('karyawan.nama',"like","%".trim(htmlentities($request->input('search')))."%")
                                               ->orderBy('id','DESC')
                                               ->paginate(trim(htmlentities($request->input('limit'))))
                                               ->appends(['limit' => trim(htmlentities($request->input('limit')))]);
            }
            else{
                $data["data"] = KaryawanModel::leftJoin('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id')
                                               ->select('karyawan.*', 'jabatan.jabatan')
                                               ->orderBy('id','DESC')
                                               ->paginate(10);       
            }
            
        }
        return view('karyawan.karyawan',$data);
    }

    public function detail(Request $request)
    {
        if(empty($request->input('id')) || $request->input('id') == null || $request->input('id') == ""){
            echo "Parameter Needed";die;
        }
        $where = [
            'karyawan.id' => trim(htmlentities($request->input('id')))
        ];
        $data["karyawan"] = KaryawanModel::where($where)
                                            ->leftJoin('jabatan', 'karyawan.id_jabatan', '=', 'jabatan.id')
                                            ->select('karyawan.*', 'jabatan.jabatan')
                                            ->get();
        if(!$data["karyawan"]){
            echo "404 Item Not Found";die;
        }
        return view('karyawan.karyawan-detail',$data);
    }
    public function create()
    {
        $data["jabatan"] = JabatanModel::all();
        return view('karyawan.karyawan-tambah',$data);
    }

    public function edit(Request $request)
    {
        if(empty($request->input('id')) || $request->input('id') == null || $request->input('id') == ""){
            echo "Parameter Needed";die;
        }
        $where = [
            'id' => trim(htmlentities($request->input('id')))
        ];
        $data["karyawan"] = KaryawanModel::where($where)->get();
        $data["jabatan"] = JabatanModel::all();
        if(!$data["karyawan"]){
            echo "404 Item Not Found";die;
        }
        return view('karyawan.karyawan-edit',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data_json = [];
        if($request->method('post')){
            if(empty($request->input('nama')) || $request->input('nama') == null || $request->input('nama') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Nama karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->file('foto')) || $request->file('foto') == null || $request->file('foto') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Foto karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('id_jabatan')) || $request->input('id_jabatan') == null || $request->input('id_jabatan') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "ID jabatan  tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('email')) || $request->input('email') == null || $request->input('email') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Email karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('no_hp')) || $request->input('no_hp') == null || $request->input('no_hp') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "No.hp karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('tanggal_lahir')) || $request->input('tanggal_lahir') == null || $request->input('tanggal_lahir') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Tanggal lahir karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('tempat_lahir')) || $request->input('tempat_lahir') == null || $request->input('tempat_lahir') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Tempat lahir karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('alamat')) || $request->input('alamat') == null || $request->input('alamat') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Alamat karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('jk')) || $request->input('jk') == null || $request->input('jk') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Gender karyawan tidak boleh kosong";
                goto ResultData;
            }
            if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Email tidak valid";
                goto ResultData;
            }
            $validExt = ['jpeg','jpg','png','gif'];

            $image = $request->file('foto');
            $ext = $image->getClientOriginalExtension();
            $path = public_path().'/images';

            if(in_array($ext, $validExt)){
                
                $new_name = rand() . '.' . $ext;
                if (!file_exists($path)){
                    mkdir($path,0777,true);
                }
                $image->move(public_path('images'), $new_name);

                $insert_data = [
                    'photo' => trim(htmlentities($new_name)),
                    'id_jabatan' => trim(htmlentities($request->id_jabatan)),
                    'nama' => trim(htmlentities($request->nama)),
                    'jk' => trim(htmlentities($request->jk)),
                    'tempat_lahir' => trim(htmlentities($request->tempat_lahir)),
                    'tanggal_lahir' => trim(htmlentities($request->tanggal_lahir)),
                    'no_hp' => trim(htmlentities($request->no_hp)),
                    'email' => trim(htmlentities(trim(htmlentities($request->email)))),
                    'alamat' => trim(htmlentities($request->alamat))
                ];
                $insert = KaryawanModel::create($insert_data);
                if($insert){
                    $data_json["IsError"] = FALSE;
                    $data_json["Message"] = "Data berhasil ditambahkan";
                    goto ResultData;
                }
                else{
                    $data_json["IsError"] = TRUE;
                    $data_json["Message"] = "Data gagal ditambahkan";
                    goto ResultData;
                }
            }
            else{
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "File hanya diperbolehkan [JPEG,JPG,GIF,PNG]";
                goto ResultData;
            }
        }
        else{
            $data_json["IsError"] = TRUE;
            $data_json["Message"] = "Invalid Request";
        }
        ResultData:
        echo json_encode($data_json);
    }
    public function update(Request $request)
    {
        $data_json = [];
        if($request->method('post')){
            if(empty($request->input('id')) || $request->input('id') == null || $request->input('id') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "ID karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('nama')) || $request->input('nama') == null || $request->input('nama') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Nama karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('id_jabatan')) || $request->input('id_jabatan') == null || $request->input('id_jabatan') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "ID jabatan  tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('email')) || $request->input('email') == null || $request->input('email') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Email karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('no_hp')) || $request->input('no_hp') == null || $request->input('no_hp') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "No.hp karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('tanggal_lahir')) || $request->input('tanggal_lahir') == null || $request->input('tanggal_lahir') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Tanggal lahir karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('tempat_lahir')) || $request->input('tempat_lahir') == null || $request->input('tempat_lahir') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Tempat lahir karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('alamat')) || $request->input('alamat') == null || $request->input('alamat') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Alamat karyawan tidak boleh kosong";
                goto ResultData;
            }
            if(empty($request->input('jk')) || $request->input('jk') == null || $request->input('jk') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Gender karyawan tidak boleh kosong";
                goto ResultData;
            }
            if (!filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Email tidak valid";
                goto ResultData;
            }
            $where = [
                'id' => trim(htmlentities($request->input('id')))
            ];

            $get = KaryawanModel::where($where)->get();

            if(!$get){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Data tidak ditemukan";
            }

            $new_name = "";
            if(empty($request->file('foto'))){
                $new_name = $get[0]->photo;
            }
            else{
                $validExt = ['jpeg','jpg','png','gif'];

                $image = $request->file('foto');
                $ext = $image->getClientOriginalExtension();
                $path = public_path().'/images';

                if(in_array($ext, $validExt)){
                    
                    $new_name = rand() . '.' . $ext;
                    if (!file_exists($path)){
                        mkdir($path,0777,true);
                    }
                    $image->move(public_path('images'), $new_name);
                    if(file_exists($path)){
                        if(is_file($path.$get[0])){
                            chmod($path.$get[0]->photo,0777);
                            unlink(public_path('images/')."/".$get[0]->photo); 
                        }
                          
                    }

                    $update_data = [
                        'photo' => trim(htmlentities($new_name)),
                        'id_jabatan' => trim(htmlentities($request->id_jabatan)),
                        'nama' => trim(htmlentities($request->nama)),
                        'jk' => trim(htmlentities($request->jk)),
                        'tempat_lahir' => trim(htmlentities($request->tempat_lahir)),
                        'tanggal_lahir' => trim(htmlentities($request->tanggal_lahir)),
                        'no_hp' => trim(htmlentities($request->no_hp)),
                        'email' => trim(htmlentities(trim(htmlentities($request->email)))),
                        'alamat' => trim(htmlentities($request->alamat))
                    ];

                    $update = KaryawanModel::where('id', trim(htmlentities($request->input('id'))))
                                   ->update($update_data);

                    if($update){
                        $data_json["IsError"] = FALSE;
                        $data_json["Message"] = "Data berhasil diubah";
                        goto ResultData;
                    }
                    else{
                        $data_json["IsError"] = TRUE;
                        $data_json["Message"] = "Data gagal diubah";
                        goto ResultData;
                    }
                }
                else{
                    $data_json["IsError"] = TRUE;
                    $data_json["Message"] = "File hanya diperbolehkan [JPEG,JPG,GIF,PNG]";
                    goto ResultData;
                }
            }
            
        }
        else{
            $data_json["IsError"] = TRUE;
            $data_json["Message"] = "Invalid Request";
        }
        ResultData:
        echo json_encode($data_json);
    }
    public function delete(Request $request){
        $data_json = [];
        if($request->method('post')){
            if(empty($request->input('id'))){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "ID karyawan tidak boleh kosong!";
                goto ResultData;
            }

            $delete = KaryawanModel::where('id', trim(htmlentities($request->input('id'))))
                                   ->delete();

            if($delete){
                $data_json["IsError"] = FALSE;
                $data_json["Message"] = "Data berhasil dihapus";
                goto ResultData;
            }
            else{
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Data gagal dihapus";
                goto ResultData;
            }
        }
        else{
            $data_json["IsError"] = TRUE;
            $data_json["Message"] = "Invalid Request";
        }
        ResultData:
        echo json_encode($data_json);
    }
}
