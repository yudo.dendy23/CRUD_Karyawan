<?php

namespace App\Http\Controllers;

use App\JabatanModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        if(!empty($request->input('search'))){
            if(!empty($request->input('limit'))){
                $data["data"] = JabatanModel::where('jabatan','like',"%".$request->input('search')."%")
                                            ->paginate(trim(htmlentities($request->input('limit'))))
                                            ->appends(['search' => $request->input('search'),'limit' => $request->input('limit')]);
            }
            else{
                $data["data"] = JabatanModel::where('jabatan','like',"%".$request->input('search')."%")
                                            ->paginate(10)
                                            ->appends('search',$request->input('search'));
            } 
        }
        else{
            if(!empty($request->input('limit'))){
                $data["data"] = JabatanModel::paginate(trim(htmlentities($request->input('limit'))))
                                            ->appends(['limit' => trim(htmlentities($request->input('limit')))]);
            }
            else{
                $data["data"] = JabatanModel::paginate(10);        
            }
            
        }
        return view('jabatan.jabatan',$data);
    }
    public function data_byId(Request $request)
    {
        $data_json = [];
        if($request->method('get')){
            if(empty($request->input('id'))){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "ID jabatan tidak boleh kosong!";
            }
            $where = [
                'id' => trim(htmlentities($request->input('id')))
            ];

            $get = JabatanModel::where($where)->get();

            if($get){
                $data_json["IsError"] = FALSE;
                $data_json["Data"] = $get->toArray();
                $data_json["Message"] = "Data ditemukan";
            }
            else{
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Data tidak ditemukan";
            }
        }
        else{
            $data_json["IsError"] = TRUE;
            $data_json["Message"] = "Invalid Request";
        }
        ResultData:
        echo json_encode($data_json);
    }
    public function store(Request $request)
    {
        $data_json = [];
        if($request->method('post')){
            if(empty($request->input('jabatan')) || $request->input('jabatan') == null || $request->input('jabatan') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Jabatan tidak boleh kosong";
                goto ResultData;
            }

            $insert_data = [
                'jabatan' => trim(htmlentities($request->input('jabatan')))
            ];

            $insert = JabatanModel::create($insert_data);

            if($insert){
                $data_json["IsError"] = FALSE;
                $data_json["Message"] = "Data berhasil ditambahkan";
                goto ResultData;
            }
            else{
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Data gagal ditambahkan";
                goto ResultData;
            }
        }
        else{
            $data_json["IsError"] = TRUE;
            $data_json["Message"] = "Invalid Request";
        }
        ResultData:
        echo json_encode($data_json);
    }
    public function update(Request $request){
        $data_json = [];
        if($request->method('post')){
            if(empty($request->input('id'))){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "ID jabatan tidak boleh kosong!";
                goto ResultData;
            }
            if(empty($request->input('jabatan')) || $request->input('jabatan') == null || $request->input('jabatan') == ""){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Jabatan tidak boleh kosong";
                goto ResultData;
            }
            $update_data = [
                'jabatan' => trim(htmlentities($request->input('jabatan')))
            ];

            $update = JabatanModel::where('id', trim(htmlentities($request->input('id'))))
                                   ->update($update_data);

            if($update){
                $data_json["IsError"] = FALSE;
                $data_json["Message"] = "Data berhasil diubah";
                goto ResultData;
            }
            else{
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Data gagal diubah";
                goto ResultData;
            }
        }
        else{
            $data_json["IsError"] = TRUE;
            $data_json["Message"] = "Invalid Request";
        }
        ResultData:
        echo json_encode($data_json);
    }
    public function delete(Request $request){
        $data_json = [];
        if($request->method('post')){
            if(empty($request->input('id'))){
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "ID jabatan tidak boleh kosong!";
                goto ResultData;
            }

            $delete = JabatanModel::where('id', trim(htmlentities($request->input('id'))))
                                   ->delete();

            if($delete){
                $data_json["IsError"] = FALSE;
                $data_json["Message"] = "Data berhasil dihapus";
                goto ResultData;
            }
            else{
                $data_json["IsError"] = TRUE;
                $data_json["Message"] = "Data gagal dihapus";
                goto ResultData;
            }
        }
        else{
            $data_json["IsError"] = TRUE;
            $data_json["Message"] = "Invalid Request";
        }
        ResultData:
        echo json_encode($data_json);
    }

    
}
