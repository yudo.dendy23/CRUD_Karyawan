<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KaryawanModel extends Model
{
    	protected $guarded = [];
        protected $table = "karyawan";
}
